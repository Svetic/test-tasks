package yashina.philin;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class BaseActivity extends AppCompatActivity {
    static final String MY_REQUESTS = "requestlist";
    static final String ORGANIZATION_REQUESTS = "organizationrequestlist";
    static final String WAITING_REQUESTS = "waitingrequestlist";

    Settings settings;
    String userName = "";
    String head = "false";
    String requestListType = MY_REQUESTS;
    Intent returnRequestListIntent;
    String token = "";
    String requestID = "0";
    String result;

    static final String API_URL = "https://lk.philin.org/index.php/rest/approve/";

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        settings = new Settings(BaseActivity.this);
        head = settings.get("head");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        menu.setGroupVisible(R.id.requests, true);
        menu.setGroupVisible(R.id.home, true);
        menu.setGroupVisible(R.id.all, true);

        if (head.equals("true")) {
            menu.setGroupVisible(R.id.manager_requests, true);
        } else {
            menu.setGroupVisible(R.id.manager_requests, false);
        }

        menu.setGroupVisible(R.id.request_actions, false);
        menu.setGroupVisible(R.id.update_requests, false);
        menu.setGroupVisible(R.id.approve_actions, false);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.auth).setTitle(R.string.auth_out);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        settings = new Settings(BaseActivity.this);
        switch (item.getItemId()) {
            case R.id.approve:
                new ApproveRequestTask().execute();
                return true;
            case R.id.auth:
                Intent authIntent = new Intent(BaseActivity.this, MainActivity.class);
                startActivity(authIntent);
                return true;
            case R.id.home:
                Intent homeIntent = new Intent(BaseActivity.this, ProfileActivity.class);
                userName = settings.get("userName");
                homeIntent.putExtra("userName", userName);
                startActivity(homeIntent);
                return true;
            case R.id.my_requests:
                Intent requestListIntent = new Intent(BaseActivity.this, RequestListActivity.class);
                startActivity(requestListIntent);
                return true;
            case R.id.organization_requests:
                Intent organizationRequestIntent = new Intent(BaseActivity.this, RequestListActivity.class);
                organizationRequestIntent.putExtra("organizationRequests", true);
                startActivity(organizationRequestIntent);
                return true;
            case R.id.reject:
                Intent rejectIntent = new Intent(BaseActivity.this, RejectActivity.class);
                startActivity(rejectIntent);
                return true;
            case R.id.return_to_list:
                returnRequestListIntent = new Intent(BaseActivity.this, RequestListActivity.class);
                requestListType = settings.get("requestListType");

                if (requestListType.equals(ORGANIZATION_REQUESTS)) {
                    returnRequestListIntent.putExtra("organizationRequests", true);
                } else if (requestListType.equals(WAITING_REQUESTS)) {
                    returnRequestListIntent.putExtra("waitingRequests", true);
                }

                startActivity(returnRequestListIntent);
                return true;
            case R.id.update:
                Intent updateRequestListIntent = new Intent(BaseActivity.this, RequestListActivity.class);
                requestListType = settings.get("requestListType");

                if (requestListType.equals(ORGANIZATION_REQUESTS)) {
                    updateRequestListIntent.putExtra("organizationRequests", true);
                } else if (requestListType.equals(WAITING_REQUESTS)) {
                    updateRequestListIntent.putExtra("waitingRequests", true);
                }

                startActivity(updateRequestListIntent);
                return true;
            case R.id.waiting_requests:
                Intent waitingRequestIntent = new Intent(BaseActivity.this, RequestListActivity.class);
                waitingRequestIntent.putExtra("waitingRequests", true);
                startActivity(waitingRequestIntent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    class ApproveRequestTask extends AsyncTask<Void, Void, String> {
        BufferedReader reader = null;
        String resultJson = "";

        protected void onPreExecute() {

        }

        protected String doInBackground(Void... urls) {
            settings = new Settings(BaseActivity.this);
            token = settings.get("token");
            requestID = settings.get("request_id");

            try {
                URL url = new URL(API_URL + requestID + "?token=" + token);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                urlConnection.connect();
                int responseCode = urlConnection.getResponseCode();

                if (responseCode == HttpURLConnection.HTTP_OK) {
                    InputStream inputStream = urlConnection.getInputStream();
                    StringBuffer buffer = new StringBuffer();
                    reader = new BufferedReader(new InputStreamReader(inputStream));
                    String line;

                    while ((line = reader.readLine()) != null) {
                        buffer.append(line);
                    }

                    resultJson = buffer.toString();
                }
            } catch (Exception e) {
                Log.i("ERROR", e.getMessage(), e);
                return null;
            }

            return resultJson;
        }

        protected void onPostExecute(String strJson) {
            super.onPostExecute(strJson);

            if (strJson.equals("")) {
                strJson = "Ошибка доступа";
                Toast toast = Toast.makeText(BaseActivity.this, strJson, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } else {
                strJson = strJson.replace("\\\"","");
                result = strJson.replace("\"","");
                Log.i("result", result);

                if (result.equals("true")) {
                    strJson = "Согласовано";
                    Toast toast = Toast.makeText(BaseActivity.this, strJson, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                    returnRequestListIntent = new Intent(BaseActivity.this, RequestListActivity.class);
                    returnRequestListIntent.putExtra("waitingRequests", true);
                    startActivity(returnRequestListIntent);
                } else {
                    strJson = "В процессе согласования произошла ошибка, попробуйте позже.";
                    Toast toast = Toast.makeText(BaseActivity.this, strJson, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        }
    }
}