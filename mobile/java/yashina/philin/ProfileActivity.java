package yashina.philin;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ProfileActivity extends BaseActivity {
    Integer waitingRequests;
    TextView waitingRequestsView;
    TextView responseView;
    String userName;
    Button queryButton;

    static final String API_URL = "https://lk.philin.org/index.php/rest/countwaitingrequests";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        userName = getIntent().getStringExtra("userName");
        responseView = (TextView) findViewById(R.id.responseView);
        responseView.setText("Добро пожаловать, " + userName + "!");
        queryButton = (Button) findViewById(R.id.queryButton);
        queryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent waitingRequestIntent = new Intent(ProfileActivity.this, RequestListActivity.class);
                waitingRequestIntent.putExtra("waitingRequests", true);
                startActivity(waitingRequestIntent);
            }
        });

        new WaitingRequestsTask().execute();
    }

    class WaitingRequestsTask extends AsyncTask<Void, Void, String> {
        BufferedReader reader = null;
        String resultJson = "";

        protected void onPreExecute() {

        }

        protected String doInBackground(Void... urls) {
            settings = new Settings(ProfileActivity.this);
            token = settings.get("token");

            try {
                URL url = new URL(API_URL + "?token=" + token);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                urlConnection.connect();
                int responseCode = urlConnection.getResponseCode();

                if (responseCode == HttpURLConnection.HTTP_OK) {
                    InputStream inputStream = urlConnection.getInputStream();
                    StringBuffer buffer = new StringBuffer();
                    reader = new BufferedReader(new InputStreamReader(inputStream));
                    String line;

                    while ((line = reader.readLine()) != null) {
                        buffer.append(line);
                    }

                    resultJson = buffer.toString();
                }
            } catch (Exception e) {
                Log.i("ERROR", e.getMessage(), e);
                return null;
            }

            return resultJson;
        }

        protected void onPostExecute(String strJson) {
            super.onPostExecute(strJson);

            if (strJson.equals("")) {
                strJson = "Ошибка доступа";
                Toast toast = Toast.makeText(ProfileActivity.this, strJson, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } else {
                strJson = strJson.replace("\\\"","");
                waitingRequests = Integer.parseInt(strJson.replace("\\\"","'"));

                if (waitingRequests > 0) {
                    waitingRequestsView = (TextView) findViewById(R.id.wRequestsView);
                    queryButton.setVisibility(View.VISIBLE);

                    if (waitingRequests == 1) {
                        waitingRequestsView.setText("Вашего решения ожидает " + waitingRequests + " заявка");
                    } else if (waitingRequests > 1 && waitingRequests < 5) {
                        waitingRequestsView.setText("Вашего решения ожидают " + waitingRequests + " заявки");
                    } else {
                        waitingRequestsView.setText("Вашего решения ожидают " + waitingRequests + " заявок");
                    }
                }
            }
        }
    }
}


