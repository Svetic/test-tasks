package yashina.philin;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class RequestViewActivity extends BaseActivity {
    String requestID;
    String canApprove;
    GridViewScrollable paymentList;
    RecyclerView fieldList;
    TextView idView;
    TextView urgentView;
    TableRow urgentRow;
    TextView serviceView;
    TextView statusView;
    TextView agreedByView;
    TableRow agreedByRow;
    TextView waitForView;
    TableRow waitForRow;
    TextView reasonView;
    TableRow reasonRow;
    TextView initiatorView;
    TextView dateCreatedView;
    TextView datePlannedView;
    TextView commentView;
    TextView contractorView;
    TableRow contractorRow;
    TextView employeeView;
    TableRow employeeRow;
    TextView summView;
    TableRow summRow;
    Menu rMenu;

    static final String API_URL = "https://lk.philin.org/index.php/rest/getrequest/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_view);
        new RequestViewTask().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        super.onCreateOptionsMenu(menu);
        menu.setGroupVisible(R.id.request_actions, true);
        rMenu = menu;

        return true;
    }

    class RequestViewTask extends AsyncTask<Void, Void, String> {
        BufferedReader reader = null;
        String resultJson = "";

        protected void onPreExecute() {

        }

        protected String doInBackground(Void... urls) {
            settings = new Settings(RequestViewActivity.this);
            token = settings.get("token");

            try {
                settings = new Settings(RequestViewActivity.this);
                token = settings.get("token");
                requestID = getIntent().getStringExtra("requestID");
                URL url = new URL(API_URL + requestID + "?token=" + token);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                urlConnection.connect();
                int responseCode = urlConnection.getResponseCode();

                if (responseCode == HttpURLConnection.HTTP_OK) {
                    InputStream inputStream = urlConnection.getInputStream();
                    StringBuffer buffer = new StringBuffer();
                    reader = new BufferedReader(new InputStreamReader(inputStream));
                    String line;

                    while ((line = reader.readLine()) != null) {
                        buffer.append(line);
                    }

                    resultJson = buffer.toString();
                }
            } catch (Exception e) {
                Log.i("ERROR", e.getMessage(), e);
                return null;
            }

            return resultJson;
        }

        protected void onPostExecute(String strJson) {
            super.onPostExecute(strJson);

            if (strJson == "") {
                strJson = "Ошибка доступа";
                Toast toast = Toast.makeText(RequestViewActivity.this, strJson, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } else {
                JSONObject obj = null;
                strJson = strJson.replace("\\\"","'");

                try {
                    obj = new JSONObject(strJson.substring(1, strJson.length()-1));
                    Request request = new Request(
                            obj.getString("service_name"),
                            obj.getString("contractor_name"),
                            obj.getString("summ"),
                            obj.getString("status"),
                            obj.getString("id"),
                            obj.getString("urgent"),
                            obj.getString("agreed_by"),
                            obj.getString("wait_for"),
                            obj.getString("reason"),
                            obj.getString("employee_name"),
                            obj.getString("initiator"),
                            obj.getString("date_created"),
                            obj.getString("date_planned"),
                            obj.getString("comment"),
                            obj.getString("has_payments"),
                            obj.getString("has_fields")
                    );

                    RequestViewActivity.this.setTitle("ID " + obj.getString("id"));

                    canApprove = obj.getString("can_approve");

                    if (canApprove.equals("true")) {
                        rMenu.setGroupVisible(R.id.approve_actions, true);
                        settings.setConfig("request_id", obj.getString("id"));
                    } else {
                        settings.setConfig("request_id", "0");
                    }

                    idView = (TextView) findViewById(R.id.id);
                    idView.setText(request.getId());

                    if (request.getUrgent().equals("false")) {
                        urgentRow = (TableRow) findViewById(R.id.urgent_row);
                        urgentRow.setVisibility(View.GONE);
                    } else {
                        urgentView = (TextView) findViewById(R.id.urgent);
                        urgentView.setText(request.getUrgent());
                    }

                    serviceView = (TextView) findViewById(R.id.service);
                    serviceView.setText(request.getServiceName());

                    statusView = (TextView) findViewById(R.id.status);
                    statusView.setText(request.getStatus());

                    if (request.getContractorName().equals("false")) {
                        contractorRow = (TableRow) findViewById(R.id.contractor_row);
                        contractorRow.setVisibility(View.GONE);
                    } else {
                        contractorView = (TextView) findViewById(R.id.contractor);
                        contractorView.setText(request.getContractorName());
                    }

                    if (request.getEmployeeName().equals("false")) {
                        employeeRow = (TableRow) findViewById(R.id.employee_row);
                        employeeRow.setVisibility(View.GONE);
                    } else {
                        employeeView = (TextView) findViewById(R.id.employee);
                        employeeView.setText(request.getEmployeeName());
                    }

                    if (request.getSumm().equals("0")) {
                        summRow = (TableRow) findViewById(R.id.summ_row);
                        summRow.setVisibility(View.GONE);
                    } else {
                        summView = (TextView) findViewById(R.id.summ);
                        summView.setText(request.getSumm());
                    }

                    if (request.getAgreedBy().equals("false")) {
                        agreedByRow = (TableRow) findViewById(R.id.agreed_by_row);
                        agreedByRow.setVisibility(View.GONE);
                    } else {
                        agreedByView = (TextView) findViewById(R.id.agreed_by);
                        agreedByView.setText(request.getAgreedBy());
                    }

                    if (request.getWaitFor().equals("false")) {
                        waitForRow = (TableRow) findViewById(R.id.wait_for_row);
                        waitForRow.setVisibility(View.GONE);
                    } else {
                        waitForView = (TextView) findViewById(R.id.wait_for);
                        waitForView.setText(request.getWaitFor());
                    }

                    if (request.getReason().equals("false")) {
                        reasonRow = (TableRow) findViewById(R.id.reason_row);
                        reasonRow.setVisibility(View.GONE);
                    } else {
                        reasonView = (TextView) findViewById(R.id.reason);
                        reasonView.setText(request.getReason());
                    }

                    initiatorView = (TextView) findViewById(R.id.initiator);
                    initiatorView.setText(request.getInitiator());

                    dateCreatedView = (TextView) findViewById(R.id.date_created);
                    dateCreatedView.setText(request.getDateCreated());

                    datePlannedView = (TextView) findViewById(R.id.date_planned);
                    datePlannedView.setText(request.getDatePlanned());

                    commentView = (TextView) findViewById(R.id.comment);
                    commentView.setText(request.getComment());

                    if (request.hasPayments().equals("true")) {
                        JSONObject payments = obj.getJSONObject("payments");
                        Iterator<String> iter = payments.keys();
                        List<Payment> paymentJson = new ArrayList<Payment>();

                        while (iter.hasNext()) {
                            String key = iter.next();
                            try {
                                JSONObject value = payments.getJSONObject(key);
                                Payment payment = new Payment(
                                        value.getString("source_name"),
                                        value.getString("bf_name"),
                                        value.getString("programm_name"),
                                        value.getString("project_name"),
                                        value.getString("event_name"),
                                        value.getString("expenditure_name"),
                                        value.getString("summ"),
                                        value.getString("currency")
                                );

                                paymentJson.add(payment);

                            } catch (JSONException e) {

                            }
                        }

                        paymentList = (GridViewScrollable) findViewById(R.id.paymentList);
                        PaymentAdapter paymentAdapter = new PaymentAdapter(RequestViewActivity.this, R.layout.payment_item, paymentJson);
                        paymentList.setAdapter(paymentAdapter);
                    }

                    if (request.hasFields().equals("true")) {
                        JSONObject fields = obj.getJSONObject("fields");
                        Iterator<String> fieldIter = fields.keys();
                        List<Field> fieldJson = new ArrayList<Field>();

                        while (fieldIter.hasNext()) {
                            String key = fieldIter.next();
                            try {
                                JSONObject value = fields.getJSONObject(key);
                                Field field = new Field(
                                        value.getString("label"),
                                        value.getString("value")
                                );

                                fieldJson.add(field);

                            } catch (JSONException e) {

                            }
                        }

                        fieldList = (RecyclerView) findViewById(R.id.fieldList);
                        fieldList.setHasFixedSize(true);
                        fieldList.setLayoutManager(new LinearLayoutManager(RequestViewActivity.this));
                        FieldAdapter fieldAdapter = new FieldAdapter(fieldJson, RequestViewActivity.this);
                        fieldList.setAdapter(fieldAdapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}


