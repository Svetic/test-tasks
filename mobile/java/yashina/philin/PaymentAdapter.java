package yashina.philin;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class PaymentAdapter extends ArrayAdapter<Payment> {
    private LayoutInflater inflater;
    private int layout;
    private List<Payment> payments;

    public PaymentAdapter(Context context, int resource, List<Payment> payments) {
        super(context, resource, payments);
        this.payments = payments;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        PaymentAdapter.ViewHolder viewHolder;

        if(convertView == null){
            convertView = inflater.inflate(this.layout, parent, false);
            viewHolder = new PaymentAdapter.ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else{
            viewHolder = (PaymentAdapter.ViewHolder) convertView.getTag();
        }

        Payment payment = payments.get(position);

        if (payment.getSumm().equals("")) {
            viewHolder.summView.setVisibility(View.GONE);
        } else {
            viewHolder.summView.setText(this.getContext().getString(R.string.summ) + " " + payment.getSumm());
        }

        if (payment.getCurrency().equals("")) {
            viewHolder.currencyView.setVisibility(View.GONE);
        } else {
            viewHolder.currencyView.setText(this.getContext().getString(R.string.currency) + " " + payment.getCurrency());
        }

        if (payment.getBfName().equals("") || payment.getBfName().equals("null")) {
            viewHolder.bfView.setVisibility(View.GONE);
        } else {
            viewHolder.bfView.setText(this.getContext().getString(R.string.bf) + " " + payment.getBfName());
        }

        if (payment.getProgrammName().equals("")) {
            viewHolder.programmView.setVisibility(View.GONE);
        } else {
            viewHolder.programmView.setText(this.getContext().getString(R.string.programm) + " " + payment.getProgrammName());
        }

        if (payment.getProjectName().equals("")) {
            viewHolder.projectView.setVisibility(View.GONE);
        } else {
            viewHolder.projectView.setText(this.getContext().getString(R.string.project) + " " + payment.getProjectName());
        }

        if (payment.getEventName().equals("")) {
            viewHolder.eventView.setVisibility(View.GONE);
        } else {
            viewHolder.eventView.setText(this.getContext().getString(R.string.event) + " " + payment.getEventName());
        }

        if (payment.getExpenditureName().equals("")) {
            viewHolder.expenditureView.setVisibility(View.GONE);
        } else {
            viewHolder.expenditureView.setText(this.getContext().getString(R.string.expenditure) + " " + payment.getExpenditureName());
        }

        if (payment.getSourceName().equals("")) {
            viewHolder.sourceView.setVisibility(View.GONE);
        } else {
            viewHolder.sourceView.setText(this.getContext().getString(R.string.source) + " " + payment.getSourceName());
        }

        return convertView;
    }
    private class ViewHolder {
        final TextView summView, currencyView, bfView, programmView, projectView, eventView, expenditureView, sourceView;

        ViewHolder(View view){
            summView = (TextView) view.findViewById(R.id.summ);
            currencyView = (TextView) view.findViewById(R.id.currency);
            bfView = (TextView) view.findViewById(R.id.bf);
            programmView = (TextView) view.findViewById(R.id.programm);
            projectView = (TextView) view.findViewById(R.id.project);
            eventView = (TextView) view.findViewById(R.id.event);
            expenditureView = (TextView) view.findViewById(R.id.expenditure);
            sourceView = (TextView) view.findViewById(R.id.source);
        }
    }
}
