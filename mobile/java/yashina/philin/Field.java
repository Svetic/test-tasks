package yashina.philin;

/**
 * Created by syashina on 12.04.2017.
 */

public class Field {
    private String label;
    private String value;

    public Field(
            String label,
            String value) {
        this.label = label;
        this.value = value;
    }

    public String getFieldLabel() {
        return label;
    }
    public String getFieldValue() {
        return value;
    }
}
