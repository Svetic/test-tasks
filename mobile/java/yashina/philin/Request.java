package yashina.philin;

/**
 * Created by syashina on 07.04.2017.
 */

public class Request {
    private String id;
    private String service_name;
    private String contractor_name;
    private String summ;
    private String status;
    private String urgent;
    private String agreed_by;
    private String wait_for;
    private String reason;
    private String employee_name;
    private String initiator;
    private String date_created;
    private String date_planned;
    private String comment;
    private String has_payments;
    private String has_fields;

    public Request(
            String service_name,
            String contractor_name,
            String summ,
            String status,
            String id,
            String initiator)
    {
        this.service_name = service_name;
        this.contractor_name = contractor_name;
        this.summ = summ;
        this.status = status;
        this.id = id;
        this.initiator = initiator;
    }

    public Request(
            String service_name,
            String contractor_name,
            String summ,
            String status,
            String id,
            String urgent,
            String agreed_by,
            String wait_for,
            String reason,
            String employee_name,
            String initiator,
            String date_created,
            String date_planned,
            String comment,
            String has_payments,
            String has_fields) {
        this.service_name = service_name;
        this.contractor_name = contractor_name;
        this.summ = summ;
        this.status = status;
        this.id = id;
        this.urgent = urgent;
        this.agreed_by = agreed_by;
        this.wait_for = wait_for;
        this.reason = reason;
        this.employee_name = employee_name;
        this.initiator = initiator;
        this.date_created = date_created;
        this.date_planned = date_planned;
        this.comment = comment;
        this.has_payments = has_payments;
        this.has_fields = has_fields;
    }

    public String getAgreedBy() {
        return agreed_by;
    }

    public String getComment() {
        return comment;
    }

    public String getDateCreated() {
        return date_created;
    }

    public String getDatePlanned() {
        return date_planned;
    }

    public String getEmployeeName() {
        return employee_name;
    }

    public String getId() {
        return id;
    }

    public String getInitiator() {
        return initiator;
    }

    public String getContractorName() {
        return contractor_name;
    }

    public String getReason() {
        return reason;
    }

    public String getServiceName() {
        return service_name;
    }

    public String getStatus() {
        return status;
    }

    public String getSumm() {
        return summ;
    }

    public String getUrgent() {
        return urgent;
    }

    public String getWaitFor() {
        return wait_for;
    }

    public String hasFields() {
        return has_fields;
    }

    public String hasPayments() {
        return has_payments;
    }
}
