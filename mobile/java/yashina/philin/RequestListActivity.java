package yashina.philin;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class RequestListActivity extends BaseActivity {
    ListView requestList;
    public Boolean organizationRequests;
    public Boolean waitingRequests;

    static final String API_URL = "https://lk.philin.org/index.php/rest/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_list);
        organizationRequests = getIntent().getBooleanExtra("organizationRequests", false);
        waitingRequests = getIntent().getBooleanExtra("waitingRequests", false);
        new RequestListTask().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        super.onCreateOptionsMenu(menu);
        menu.setGroupVisible(R.id.update_requests, true);

        return true;
    }

    class RequestListTask extends AsyncTask<Void, Void, String> {
        BufferedReader reader = null;
        String resultJson = "";

        protected void onPreExecute() {

        }

        protected String doInBackground(Void... urls) {
            settings = new Settings(RequestListActivity.this);
            token = settings.get("token");

            try {
                String requestMethod = MY_REQUESTS;

                if (organizationRequests == true) {
                    requestMethod = ORGANIZATION_REQUESTS;
                    RequestListActivity.this.setTitle(RequestListActivity.this.getString(R.string.organization_requests));
                } else if (waitingRequests == true) {
                    requestMethod = WAITING_REQUESTS;
                    RequestListActivity.this.setTitle(RequestListActivity.this.getString(R.string.waiting_requests));
                }

                settings.setConfig("requestListType", requestMethod);

                URL url = new URL(API_URL + requestMethod + "?token=" + token);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                urlConnection.connect();
                int responseCode = urlConnection.getResponseCode();

                if (responseCode == HttpURLConnection.HTTP_OK) {
                    InputStream inputStream = urlConnection.getInputStream();
                    StringBuffer buffer = new StringBuffer();
                    reader = new BufferedReader(new InputStreamReader(inputStream));
                    String line;

                    while ((line = reader.readLine()) != null) {
                        buffer.append(line);
                    }

                    resultJson = buffer.toString();
                }
            } catch (Exception e) {
                Log.i("ERROR", e.getMessage(), e);
                return null;
            }

            return resultJson;
        }

        protected void onPostExecute(String strJson) {
            super.onPostExecute(strJson);

            if (strJson.equals("")) {
                strJson = "Ошибка доступа";
                Toast toast = Toast.makeText(RequestListActivity.this, strJson, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } else {
                JSONObject obj = null;
                strJson = strJson.replace("\\\"","'");

                try {
                    obj = new JSONObject(strJson.substring(1, strJson.length()-1));

                    try {
                        JSONObject requests = obj.getJSONObject("requests");
                        Iterator<String> iter = requests.keys();
                        List<Request> requestJson = new ArrayList<Request>();

                        while (iter.hasNext()) {
                            String key = iter.next();
                            try {
                                JSONObject value = requests.getJSONObject(key);
                                Request request = new Request(
                                        value.getString("service_name"),
                                        value.getString("contractor_name"),
                                        value.getString("summ"),
                                        value.getString("request_status"),
                                        key,
                                        value.getString("initiator")
                                );

                                requestJson.add(request);

                            } catch (JSONException e) {

                            }
                        }

                        requestList = (ListView) findViewById(R.id.requestList);
                        RequestAdapter requestAdapter = new RequestAdapter(RequestListActivity.this, R.layout.list_item, requestJson);
                        requestList.setAdapter(requestAdapter);

                        AdapterView.OnItemClickListener itemListener = new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                                // получаем выбранный пункт
                                Request selectedRequest = (Request)parent.getItemAtPosition(position);
                                Intent requestIntent = new Intent(RequestListActivity.this, RequestViewActivity.class);
                                requestIntent.putExtra("requestID", selectedRequest.getId());
                                startActivity(requestIntent);
                            }
                        };
                        requestList.setOnItemClickListener(itemListener);
                    } catch (JSONException e) {
                        requestList = (ListView) findViewById(R.id.requestList);
                        requestList.setEmptyView(findViewById(R.id.emptyElement));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
