package yashina.philin;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class RejectActivity extends BaseActivity {

    EditText reasonText;
    TextView requestIDView;
    ProgressBar progressBar;
    Button queryButton;

    static final String API_URL = "https://lk.philin.org/index.php/rest/reject/";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reject);
        settings = new Settings(RejectActivity.this);
        requestID = settings.get("request_id");
        reasonText = (EditText) findViewById(R.id.reasonText);
        requestIDView = (TextView) findViewById(R.id.requestIDView);
        requestIDView.setText("Причина отклонения заявки ID " + requestID + ":");
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        queryButton = (Button) findViewById(R.id.queryButton);
        queryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new RetrieveFeedTask().execute();
            }
        });
    }

    class RetrieveFeedTask extends AsyncTask<Void, Void, String> {
        BufferedReader reader = null;
        String resultJson = "";

        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        protected String doInBackground(Void... urls) {
            String reason = reasonText.getText().toString();
            token = settings.get("token");

            try {
                URL url = new URL(API_URL + requestID );
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Authorization", "Bearer " + token);
                HashMap<String, String> postDataParams =  new HashMap<String, String>();
                postDataParams.put("reason", reason);

                OutputStream os = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));
                writer.flush();
                writer.close();
                os.close();

                urlConnection.connect();
                int responseCode = urlConnection.getResponseCode();

                if (responseCode == HttpURLConnection.HTTP_OK) {
                    InputStream inputStream = urlConnection.getInputStream();
                    StringBuffer buffer = new StringBuffer();
                    reader = new BufferedReader(new InputStreamReader(inputStream));
                    String line;

                    while ((line = reader.readLine()) != null) {
                        buffer.append(line);
                    }

                    resultJson = buffer.toString();
                }
            } catch (Exception e) {
                Log.i("ERROR", e.getMessage(), e);
                return null;
            }

            return resultJson;
        }

        protected void onPostExecute(String strJson) {
            super.onPostExecute(strJson);
            progressBar.setVisibility(View.GONE);

            if (strJson.equals("")) {
                strJson = "Ошибка доступа";
                Toast toast = Toast.makeText(RejectActivity.this, strJson, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } else {
                strJson = strJson.replace("\\\"","");
                result = strJson.replace("\"","");
                Log.i("result", result);

                if (result.equals("true")) {
                    strJson = "Заявка отклонена";
                    Toast toast = Toast.makeText(RejectActivity.this, strJson, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                    returnRequestListIntent = new Intent(RejectActivity.this, RequestListActivity.class);
                    returnRequestListIntent.putExtra("waitingRequests", true);
                    startActivity(returnRequestListIntent);
                } else {
                    strJson = "В процессе отклонения заявки произошла ошибка, попробуйте позже.";
                    Toast toast = Toast.makeText(RejectActivity.this, strJson, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        }

        private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
            StringBuilder result = new StringBuilder();
            boolean first = true;

            for(Map.Entry<String, String> entry : params.entrySet()){
                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            }

            return result.toString();
        }
    }
}