package yashina.philin;

/**
 * Created by syashina on 12.04.2017.
 */

public class Payment {
    private String source_name;
    private String bf_name;
    private String programm_name;
    private String project_name;
    private String event_name;
    private String expenditure_name;
    private String summ;
    private String currency;

    public Payment(
            String source_name,
            String bf_name,
            String programm_name,
            String project_name,
            String event_name,
            String expenditure_name,
            String summ,
            String currency) {
        this.source_name = source_name;
        this.bf_name = bf_name;
        this.programm_name = programm_name;
        this.project_name = project_name;
        this.event_name = event_name;
        this.expenditure_name = expenditure_name;
        this.summ = summ;
        this.currency = currency;
    }

    public String getSourceName() {
        return source_name;
    }

    public String getBfName() {
        return bf_name;
    }

    public String getProgrammName() {
        return programm_name;
    }

    public String getProjectName() {
        return project_name;
    }

    public String getEventName() {
        return event_name;
    }

    public String getExpenditureName() {
        return expenditure_name;
    }

    public String getSumm() {
        return summ;
    }

    public String getCurrency() {
        return currency;
    }
}

