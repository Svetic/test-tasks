package yashina.philin;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.util.Base64;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class MainActivity extends AppCompatActivity {

    EditText loginText;
    EditText passwordText;
    TextView responseView;
    ProgressBar progressBar;
    Button queryButton;
    Boolean isAuthorized = false;
    String userName = "";
    String token = "";
    String head = "";
    private Settings settings;

    static final String API_URL = "https://lk.philin.org/index.php/rest/login";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        responseView = (TextView) findViewById(R.id.responseView);
        loginText = (EditText) findViewById(R.id.loginText);
        passwordText = (EditText) findViewById(R.id.passwordText);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        queryButton = (Button) findViewById(R.id.queryButton);
        queryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new RetrieveFeedTask().execute();
            }
        });
    }

    class RetrieveFeedTask extends AsyncTask<Void, Void, String> {
        BufferedReader reader = null;
        String resultJson = "";

        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            responseView.setText("");
        }

        protected String doInBackground(Void... urls) {
            String login = loginText.getText().toString();
            String password = passwordText.getText().toString();

            try {
                URL url = new URL(API_URL);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                String userpassword = login + ":" + password;
                String encodedAuthorization = Base64.encodeToString(userpassword.getBytes("UTF-8"), Base64.DEFAULT);
                urlConnection.setRequestProperty("Authorization", "Basic " + encodedAuthorization);
                urlConnection.setRequestProperty("Content-Type", "application/json");
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.connect();
                int responseCode = urlConnection.getResponseCode();

                if (responseCode == HttpURLConnection.HTTP_OK) {
                    InputStream inputStream = urlConnection.getInputStream();
                    StringBuffer buffer = new StringBuffer();
                    reader = new BufferedReader(new InputStreamReader(inputStream));
                    String line;

                    while ((line = reader.readLine()) != null) {
                        buffer.append(line);
                    }

                    resultJson = buffer.toString();
                }
            } catch (Exception e) {
                Log.i("ERROR", e.getMessage(), e);
                return null;
            }

            return resultJson;
        }

        protected void onPostExecute(String strJson) {
            super.onPostExecute(strJson);

            if (strJson.equals("")) {
                strJson = "Ошибка доступа";
                progressBar.setVisibility(View.GONE);
                Toast toast = Toast.makeText(MainActivity.this, strJson, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } else {
                isAuthorized = true;
                JSONObject obj = null;
                strJson = strJson.replace("\\\"","'");

                try {
                    obj = new JSONObject(strJson.substring(1, strJson.length()-1));
                    userName = obj.getString("userName");
                    token = obj.getString("token");
                    head = obj.getString("head");
                    settings = new Settings(MainActivity.this);
                    settings.setConfig("token", token);
                    settings.setConfig("userName", userName);
                    settings.setConfig("head", head);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                intent.putExtra("userName", userName);
                startActivity(intent);
            }
        }
    }
}