package yashina.philin;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;

public class Settings {
    private SharedPreferences sharedPref;

    public Settings(AppCompatActivity activity) {
        sharedPref = activity.getSharedPreferences("main", Context.MODE_PRIVATE);
    }

    public String get(String string){
        return sharedPref.getString(string, "");
    }

    public void setConfig(String string, String value){
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(string, value);
        editor.commit();
    }
}
