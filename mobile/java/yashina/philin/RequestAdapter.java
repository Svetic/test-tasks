package yashina.philin;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class RequestAdapter extends ArrayAdapter<Request> {

    private LayoutInflater inflater;
    private int layout;
    private List<Request> requests;

    public RequestAdapter(Context context, int resource, List<Request> requests) {
        super(context, resource, requests);
        this.requests = requests;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if(convertView == null){
            convertView = inflater.inflate(this.layout, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Request request = requests.get(position);

        viewHolder.serviceNameView.setText(request.getServiceName());
        viewHolder.idView.setText(request.getId());
        viewHolder.statusView.setText(request.getStatus());

        if (!request.getContractorName().equals("null")) {
            viewHolder.contractorView.setText(this.getContext().getString(R.string.contractor) + " " + request.getContractorName());
        } else {
            viewHolder.contractorView.setText("");
        }

        viewHolder.initiatorView.setText(request.getInitiator());

        if (!request.getSumm().equals("0")) {
            viewHolder.summView.setText(this.getContext().getString(R.string.summ)  + " " + request.getSumm());
        } else {
            viewHolder.summView.setText("");
        }

        return convertView;
    }
    private class ViewHolder {
        final TextView serviceNameView, idView, statusView, contractorView, summView, initiatorView;

        ViewHolder(View view){
            serviceNameView = (TextView) view.findViewById(R.id.name);
            idView = (TextView) view.findViewById(R.id.id);
            statusView = (TextView) view.findViewById(R.id.status);
            contractorView = (TextView) view.findViewById(R.id.contractor);
            summView = (TextView) view.findViewById(R.id.summ);
            initiatorView = (TextView) view.findViewById(R.id.initiator);
        }
    }
}