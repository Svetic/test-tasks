package yashina.philin;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class FieldAdapter extends RecyclerView.Adapter<FieldAdapter.ViewHolder> {

    private List<Field> listItems;
    private Context mContext;

    public FieldAdapter(List<Field> listItems, Context mContext) {
        this.listItems = listItems;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.field_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final Field itemList = listItems.get(position);
        holder.txtLabel.setText(itemList.getFieldLabel());
        holder.txtValue.setText(itemList.getFieldValue());
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView txtLabel;
        public TextView txtValue;
        public ViewHolder(View itemView) {
            super(itemView);
            txtLabel = (TextView) itemView.findViewById(R.id.label);
            txtValue = (TextView) itemView.findViewById(R.id.value);
        }
    }
}